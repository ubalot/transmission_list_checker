from torrent import (
    get_movie_title,
    get_tvshow_title,
    get_year_from_movie,
    normalize,
    audio_qualities_rgx,
    link_ads_rgx,
    movie_version_rgx,
    movie_rgx,
    tvshow_rgx,
    video_qualities_rgx,
    year_rgx,
    wrapped_year_rgx,
)


def test_tvshows_regex():
    original_name = "Stranger.Things.S01.1080p.BluRay.x265-RARBG"
    normalized_name = normalize(original_name)
    assert tvshow_rgx.search(normalized_name) is not None


def test_year_regex():
    year = year_rgx.search("1080p")
    assert year is None

    year = year_rgx.search("10801")
    assert year is None

    original_name = "Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv"
    normalized_name = normalize(original_name)
    m = year_rgx.search(normalized_name)
    assert m is not None
    year = get_year_from_movie(normalized_name)
    assert year == "2019"

    original_name = "2019.After.The.Fall.Of.New.York.1983.1080p.BluRay.x264.mkv"
    normalized_name = normalize(original_name)
    m = year_rgx.search(normalized_name)
    assert m is not None
    year = get_year_from_movie(normalized_name)
    assert year == "1983"

    original_name = "2012 (2009).mkv"
    normalized_name = normalize(original_name)
    m = year_rgx.search(normalized_name)
    assert m is not None
    year = get_year_from_movie(normalized_name)
    assert year == "2009"


def test_wrapped_year_regex():
    year = "1970"
    assert wrapped_year_rgx.search(year) is None

    year = "(1970)"
    assert wrapped_year_rgx.search(year) is not None


def test_movie_regex():
    original_name = "Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv"
    normalized_name = normalize(original_name)
    assert movie_rgx.search(normalized_name) is not None


def test_video_quality_regex():
    original_name = "Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv"
    assert video_qualities_rgx.search(original_name) is not None


def test_audio_quality_regex():
    original_name = "Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv"
    assert audio_qualities_rgx.search(original_name) is not None


def test_link_ads_regex():
    original_name = (
        "www.torrents.com - Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv"
    )
    assert link_ads_rgx.search(original_name) is not None


def test_movie_version_regex():
    original_name = "Aliens - EXTENDED (1986) 1080p H265 eng AC3 5.1 sub eng Licdom.mkv"
    assert movie_version_rgx.search(original_name) is not None


def test_movie_title():
    original_name = "Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv"

    normalized_name = normalize(original_name)
    assert normalized_name == "parasite (2019) kor ddn.mkv"

    title = get_movie_title(normalized_name)
    assert title == "parasite (2019)"

    original_name = "[HD-720p] Monsters Inc (2001) - AC3-SUB.mkv"
    normalized_name = normalize(original_name)
    title = get_movie_title(normalized_name)
    assert title == "monsters inc (2001)"


def test_tvshows_title():
    original_name = "Stranger.Things.S01.1080p.BluRay.x265-RARBG"

    normalized_name = normalize(original_name)
    assert normalized_name == "stranger things s01 -rarbg"

    title = get_tvshow_title(normalized_name)
    assert title == "stranger things s1"

    original_name = "Breaking Bad S01 1080p BluRay AV1 Opus [AV1D]"
    normalized_name = normalize(original_name)
    title = get_tvshow_title(normalized_name)
    assert title == "breaking bad s1"


def test_tvshows_title_1():
    original_name = "Ash vs Evil Dead (2015) Season 1 S01(1080p NF WEB-DL x265 HEVC 10bit EAC3 5.1 Ghost)"

    normalized_name = normalize(original_name)
    assert normalized_name == "ash vs evil dead (2015) season 1 s01( 10bit ghost)"

    title = get_tvshow_title(normalized_name)
    assert title == "ash vs evil dead (2015) s1"


def test_year_normalized_in_title():
    original_name1 = "Mother! (2017).mkv"
    original_name2 = "Mother! 2017.avi"

    normalized_name1 = normalize(original_name1)
    normalized_name2 = normalize(original_name2)

    title1 = get_movie_title(normalized_name1)
    title2 = get_movie_title(normalized_name2)
    assert title1 == title2
