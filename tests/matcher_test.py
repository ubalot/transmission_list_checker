from matcher import Matcher
from torrent import TorrentWrapper, normalize


class TorrentMock(TorrentWrapper):
    def __init__(self, name: str, num_files: int) -> None:
        self._original_name = name
        self._normalized_name = normalize(self._original_name)

        self._is_music = False
        self._is_movie = False
        self._is_tvshow = False

        self._files = ["" for _ in range(num_files)]

        if not self._is_music:
            if len(self._files) == 1:
                self._is_movie = True
            elif len(self._files) > 1:
                self._is_tvshow = True


def test_same_torrent():
    torrent1 = TorrentMock("Stranger.Things.S01.1080p.BluRay.x265-RARBG", 8)
    torrent2 = TorrentMock("Stranger.Things.S01.1080p.BluRay.x265-RARBG", 8)
    matcher = Matcher(torrent1, torrent2)
    assert matcher.distance == 0

    torrent1 = TorrentMock("Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv", 1)
    torrent2 = TorrentMock("Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv", 1)
    matcher = Matcher(torrent1, torrent2)
    assert matcher.distance == 0


def test_distance_0_torrents():
    torrent1 = TorrentMock("Stranger.Things.Season01.1080p.BluRay.x265-RARBG", 8)
    torrent2 = TorrentMock("Stranger.Things.S01.1080p.BluRay.x265-RARBG", 8)
    matcher = Matcher(torrent1, torrent2)
    assert matcher.distance == 0

    torrent1 = TorrentMock("Parasite (2019)  HD 720p DTS AC3 AC3 KOR x264 - DDN.mkv", 1)
    torrent2 = TorrentMock("Parasite (2019)", 1)
    matcher = Matcher(torrent1, torrent2)
    assert matcher.distance == 0


def test_distance_1_torrents():
    torrent1 = TorrentMock("Breaking Bad S01 1080p BluRay AV1 Opus [AV1D]", 7)
    torrent2 = TorrentMock("Breaking.Bad.S02.1080p.BluRay.x265-RARBG", 13)
    matcher = Matcher(torrent1, torrent2)
    assert matcher.distance == 1
