#!/usr/bin/env bash

case "$1" in
    black)
        uv run black .
        exit $?
        ;;

    tests)
        uv run pytest
        exit $?
        ;;
esac

uv run python transmission_torrent_list_manager/main.py "$@"
exit $?
