# Transmission list checker

### Development

#### Install dependencies
```bash
uv install
```

#### Update dependencies
```bash
uv lock --upgrade
```

#### Run tests
```bash
uv run pytest
```

#### Format project with Black
```bash
uv run black .
```

#### Run mypy check on project
```bash
uv run mypy --strict -m transmission_torrent_list_manager
```

#### Run project
```bash
./run.sh
```
