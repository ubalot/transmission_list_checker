#!/usr/bin/env python3

""" How to use this script:
Put the script and UNCHECKED_FILE in the same directory.
The script needs to be in the same system as TRANSMISSION_DIR.
When the script runs it removes all files listed in UNCHECKED_FILE,
empty dir will be removed as well.
"""

from pathlib import Path
import sys


UNCHECKED_FILE = "unchecked_files.txt"
TRANSMISSION_COMPLETE_DIR = "/mnt/HD8T/Transmission/Downloads/complete"
TRANSMISSION_INCOMPLETE_DIR = "/mnt/HD8T/Transmission/Downloads/incomplete"


def main() -> None:
    source_file = Path(UNCHECKED_FILE)
    print(source_file)
    if not source_file.is_file():
        print(f"{UNCHECKED_FILE} is not a file")
        sys.exit(1)

    transmission_complete = Path(TRANSMISSION_COMPLETE_DIR)
    if not transmission_complete.is_dir():
        print(f"{TRANSMISSION_COMPLETE_DIR} is not a directory")
        sys.exit(1)

    transmission_incomplete = Path(TRANSMISSION_INCOMPLETE_DIR)
    if not transmission_incomplete.is_dir():
        print(f"{TRANSMISSION_INCOMPLETE_DIR} is not a directory")
        sys.exit(1)

    with open(source_file, "r") as srcf:
        for line in srcf.readlines():
            partial_path = Path(line.rstrip())  # avoid newline at the end
            target_path = transmission_complete / partial_path
            if not target_path.exists():
                target_path = transmission_incomplete / Path(partial_path)
                if not target_path.exists():
                    print(f"Path doesn't exist: {partial_path}")
                    continue

            # remove file and all subdirs in the path recursively
            # until it reaches the torrent root folder
            torrent_dir = Path(partial_path.parts[0])
            p = target_path
            while p != torrent_dir:
                path = target_path / p
                if path.is_dir():
                    if len(list(path.iterdir())) == 0:
                        path.rmdir()
                    else:
                        break
                else:
                    path.unlink()
                p = p.parent


if __name__ == "__main__":
    main()
