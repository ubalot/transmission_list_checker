from argparse import ArgumentParser, Namespace


def parse_args() -> Namespace:
    parser = ArgumentParser()

    action_group = parser.add_mutually_exclusive_group()
    list_subgroup = parser.add_mutually_exclusive_group()

    action_group.add_argument(
        "-c",
        "--clean",
        help="remove unwanted files (everything that is not video related) from all torrents",
        action="store_true",
    )

    action_group.add_argument(
        "-l", "--list", help="list all torrents", action="store_true"
    )
    list_subgroup.add_argument("--movies", help="List only movies", action="store_true")
    list_subgroup.add_argument(
        "--tvshows", help="List only tv-shows", action="store_true"
    )
    parser.add_argument("--size", type=int, help="List only above size (G)")

    parser.add_argument(
        "--incomplete", help="List incomplete torrent too", action="store_true"
    )

    action_group.add_argument(
        "-s", "--similars", help="List all similar torrents", action="store_true"
    )

    parser.add_argument("-v", "--verbose", help="Run verbosely", action="store_true")

    parser.add_argument("-n", "--dry", help="Run in dry-mode", action="store_true")

    args = parser.parse_args()

    if not args.clean and not args.list and not args.similars:
        parser.error("No action requested, clean or list or similars must be selected")

    return args
