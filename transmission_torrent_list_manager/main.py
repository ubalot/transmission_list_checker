from argparse import Namespace
from itertools import combinations
import logging
import math
from pathlib import Path
import re
import signal
import sys
import time
from types import FrameType
from typing import NoReturn, Optional

from dotenv import dotenv_values
from transmission_rpc import Client
from transmission_rpc.error import TransmissionConnectError
import urllib3

from arg_parser import parse_args
from file_extensions import unwanted_extensions, wanted_extensions
from matcher import Matcher
from torrent import TorrentWrapper as Torrent


""" This var is useful for scripts in /utils, keep it in a visible position. """
UNCHECKED_FILE = Path("misc/unchecked_files.txt").resolve()


def clean_torrents(client: Client, args: Namespace) -> None:
    if not args.dry:
        if UNCHECKED_FILE.exists():
            UNCHECKED_FILE.unlink()

    report = []

    # file patterns
    sample_pattern = re.compile(r"[\s_]?(sample)\.[\w]{3}$", re.IGNORECASE)

    # directory patterns
    extras_dir_pattern = re.compile(r"/extra(s)?/", re.IGNORECASE)
    featurettes_dir_pattern = re.compile(r"/\.?_*(featurettes)/", re.IGNORECASE)
    pad_dir_pattern = re.compile(r"/\.?_*(pad|padding_file)/", re.IGNORECASE)
    sample_dir_pattern = re.compile(r"/sample.*/", re.IGNORECASE)
    subtitles_dir_pattern = re.compile(r"/sub(title)?s.*/", re.IGNORECASE)

    for t in client.get_torrents():
        if args.incomplete or t.done_date is not None:
            if t.error != 0:
                logging.warning(f"Torrent: {t.name} ### Error: {t.error_string}")

            files = t.get_files()

            wanted_files = [
                f for f in files if Path(f.name).suffix not in unwanted_extensions
            ]
            if len(wanted_files) == 0:
                logging.warning(f"Torrent has only unwanted files: {t.name}")

            for f in files:
                file_name = f.name
                extension = Path(file_name).suffix

                # remove unwanted files by extension
                #   or by filename if it ends with 'sample'
                #                   or it is some kind of 'extras'
                if (
                    extension in unwanted_extensions
                    or sample_pattern.search(file_name)
                    or extras_dir_pattern.search(file_name)
                    or pad_dir_pattern.search(file_name)
                    or featurettes_dir_pattern.search(file_name)
                    or sample_dir_pattern.search(file_name)
                    or subtitles_dir_pattern.search(file_name)
                ):
                    if f.selected:
                        if t.progress == 0.0:
                            logging.info(f"uncheck file (empty): {f.name}")
                        else:
                            logging.info(f"uncheck file: {f.name}")

                        if not args.dry:
                            client.change_torrent(t.id, files_unwanted=[f.id])

                            # don't report if download never started
                            if not args.verbose and t.progress > 0.0:
                                report.append(f.name)

                        if args.verbose:
                            input("wait for Enter...")

                # warning if file extension is unknown
                elif extension not in wanted_extensions:
                    logging.warning(
                        f"Unmanaged file extension: {extension}"
                        f"    for file {f.name}"
                    )
                    if args.verbose:
                        input("wait for Enter...")

    if not args.dry:
        with open(UNCHECKED_FILE, "w") as uf:
            uf.write("\n".join(report))
            logging.info(f"file created: {UNCHECKED_FILE}")


def list_torrents(client: Client, args: Namespace) -> None:
    torrents = []
    for t in client.get_torrents():
        if args.incomplete or t.done_date is not None:
            torrent = Torrent(t)
            if args.movies and not torrent.is_movie():
                continue
            if args.tvshows and not torrent.is_tvshow():
                continue
            if args.size and torrent.size_in_giga_bytes < args.size:
                continue
            torrents.append(torrent)

    # left pad value for torrent size
    if len(torrents) == 0:
        padding = 1
    else:
        padding = max(len(str(t.size_in_giga_bytes)) for t in torrents)

    if args.verbose:
        print("Size (G) | Torrent")
    for t in torrents:
        print(f"{t.size_in_giga_bytes:>{padding}} | {t.original_name}")


def find_similars(torrents: list[Torrent], threshold: int) -> set[Matcher]:
    similars = set()
    for torrent1, torrent2 in combinations(torrents, 2):
        matcher = Matcher(torrent1, torrent2)
        if matcher.distance > threshold or matcher.is_movie_special_case():
            continue

        if matcher not in similars:
            similars.add(matcher)

    return similars


def print_report(matchers: set[Matcher]) -> None:
    for matcher in matchers:
        print(
            f"""\
target1: {matcher.target1.original_name}
target2: {matcher.target2.original_name}
distance: {matcher.distance}
"""
        )


def print_similar_movies(movies: list[Torrent], args: Namespace) -> None:
    similar_movies = find_similars(movies, 1)
    if len(similar_movies) > 0:
        if args.verbose:
            print("Movies matches:")
        print()
        print_report(similar_movies)


def print_similar_tvshows(tvshows: list[Torrent], args: Namespace) -> None:
    similar_tvshows = find_similars(tvshows, 0)
    if len(similar_tvshows) > 0:
        if args.verbose:
            print("TvShows matches:")
        print()
        print_report(similar_tvshows)


def print_similars_torrents(client: Client, args: Namespace) -> None:
    torrents = [
        Torrent(t)
        for t in client.get_torrents()
        if args.incomplete or t.done_date is not None
    ]
    movies = [t for t in torrents if t.is_movie()]
    print_similar_movies(movies, args)
    tvshows = [t for t in torrents if t.is_tvshow()]
    print_similar_tvshows(tvshows, args)


def signal_handler(_signal: int, _frame: Optional[FrameType]) -> NoReturn:
    print(type(_signal), type(_frame))
    print("\nUser exited.")
    sys.exit(0)


def warn_user_about_previous_cleanup() -> None:
    """
    Write to user that a cleanup should be run before going on.
    """
    start = math.ceil(time.time())
    while (elapsed := math.ceil(time.time()) - start) <= 10:
        seconds = " ".join(str(i) for i in range(1, elapsed + 1))
        print(
            f"torrent cleanup should be run before this for a better experience... {seconds}",
            end="\r",
            flush=True,
        )
        time.sleep(1)
    print(" " * 120, end="\r")  # clean previous message


def main() -> None:
    signal.signal(signal.SIGINT, signal_handler)

    args = parse_args()

    FORMAT = "%(asctime)s [%(levelname)s] (%(name)s) %(message)s"
    logging.basicConfig(
        encoding="utf-8",
        format=FORMAT,
        level=logging.DEBUG if args.verbose else logging.INFO,
    )

    msg = (f"{arg}: {val}" for arg, val in vars(args).items())
    tabbed_msg = (f"\t{m}" for m in msg)
    inline_msg = "\n".join(tabbed_msg)
    logging.debug(f"args: \n{inline_msg}")

    config = dotenv_values(".env")
    if not "HOST" in config or config["HOST"] is None:
        host = "127.0.0.1"
    else:
        host = config["HOST"]

    if not "PORT" in config or config["PORT"] is None:
        port = 9091
    else:
        port = int(config["PORT"])

    if not "USERNAME" in config or config["USERNAME"] is None:
        username = ""
    else:
        username = config["USERNAME"]

    if not "PASSWORD" in config or config["PASSWORD"] is None:
        password = ""
    else:
        password = config["PASSWORD"]

    try:
        client = Client(host=host, port=port, username=username, password=password)
    except TransmissionConnectError:
        print(f"no connection to transmission service {host}:{port}")
        sys.exit(1)
    except (urllib3.exceptions.IncompleteRead, ConnectionRefusedError) as e:
        print(f"no connection to transmission service: {e}")
        sys.exit(2)

    if args.list:
        warn_user_about_previous_cleanup()
        list_torrents(client, args)
    elif args.clean:
        clean_torrents(client, args)
    elif args.similars:
        warn_user_about_previous_cleanup()
        print_similars_torrents(client, args)


if __name__ == "__main__":
    main()
