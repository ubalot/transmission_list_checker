## Usage

### Torrents clean up
Clean up of useless files in torrents that are movies or tvshows
```bash
./run -c
```

### List torrents
List all completed torrents
```bash
./run -l
```

List all torrents
```bash
./run -l --incomplete
```

List only completed movies
```bash
./run -l --movies
```

List only completed tvshows
```bash
./run -l --tvshows
```

### Filter torrents by size
List all completed torrents of size >= 5G
```bash
./run -l --size 5
```
