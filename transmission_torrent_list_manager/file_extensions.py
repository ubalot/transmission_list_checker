def __lower_and_upper(_list: list[str]) -> list[str]:
    _list.extend([e.upper() for e in _list])
    return _list


movie_extensions = __lower_and_upper([".avi", ".mkv", ".mp4"])
music_extensions = __lower_and_upper([".flac", ".mp3"])


# desirable files
wanted_extensions = movie_extensions + music_extensions

# files that should not be downloaded
unwanted_extensions = __lower_and_upper(
    [
        # text formats
        ".txt",
        ".srt",
        # images formats
        ".png",
        ".jpg",
        ".jpeg",
        # executable formats
        ".exe",
        # DB/config formats
        ".db",
        ".nfo",
        ".sr",
        ".sqlite",
        ".xml",
        # playlist formats
        ".m3u",
        # archive format
        ".rar",
        # extra/misc formats
        ".log",
        ".idx",
        ".sup",
        ".sub",
        ".website",
    ]
)
