from types import NotImplementedType
from Levenshtein import distance as damerau_levenshtein_distance

from torrent import TorrentWrapper as Torrent


class Matcher:
    """
    Every istance keep a reference to two torrents and its able to discover if they are similars.
    """

    def __init__(self, torrent1: Torrent, torrent2: Torrent) -> None:
        self._target1 = torrent1
        self._target2 = torrent2
        self._distance = damerau_levenshtein_distance(
            self.target1.title, self.target2.title
        )

    @property
    def target1(self):
        return self._target1

    @property
    def target2(self):
        return self._target2

    @property
    def distance(self):
        return self._distance

    # special case for movies:
    # no match if same title but different year
    def is_movie_special_case(self) -> bool:
        are_movies = self.target1.is_movie() and self.target2.is_movie()
        year1 = self.target1.year
        year2 = self.target2.year
        different_years = False
        if year1 is not None and year2 is not None:
            different_years = year1 != year2
        return are_movies and different_years

    def __eq__(self, other: object) -> bool | NotImplementedType:
        if not isinstance(other, Matcher):
            return NotImplemented
        st1 = self.target1.title
        st2 = self.target2.title
        ot1 = other.target1.title
        ot2 = other.target2.title
        return (st1 == ot1 and st2 == ot2) or (st1 == ot2 and st2 == ot1)

    def __hash__(self) -> int:
        return hash(self.target1) + hash(self.target2)
