import logging
from pathlib import Path
import re
from types import NotImplementedType

from transmission_rpc import Torrent

from file_extensions import music_extensions


tvshow_rgx = re.compile(
    r".+s(eason)?[\.\s-]?\d{1,2}",
    re.IGNORECASE,
)

year_rgx = re.compile(r"(?<=\W)\d{4}(?=$|\W)", re.IGNORECASE)
wrapped_year_rgx = re.compile(r"\(\d{4}\)", re.IGNORECASE)
movie_rgx = re.compile(r".*" + year_rgx.pattern + r"\)?", re.IGNORECASE)

video_qualities_rgx = re.compile(
    r"[\(-]?(720p?)|(1080p?)|((h|x)26(4|5))|(AAC)|(AC3)|([^\.]AV(I|1))|(BDMux)|(BDRip)|(BRMux)|(BRRip)|(BluRay)|(DVDRip)|(E-?AC3)|(Full-?HD)|(HD)|(HEVC)|(WEB-?DL)|(WEBRip)|(XVid)[\)-]?",
    re.IGNORECASE,
)

audio_qualities_rgx = re.compile(
    r"[\(-]?(DTS)|(NF)|(OPUS)|(5\.1)[\)-]?",
    re.IGNORECASE,
)

link_ads_rgx = re.compile(
    r"www\..+\.\w{3}",
    re.IGNORECASE,
)

movie_version_rgx = re.compile(
    r"[\(-]?(extended)|(extended cut)|(repack)|(remastered)|(subbed)|(unrated)|(uncut)[\)-]?",
    re.IGNORECASE,
)


def guess_separator(torrent_name: str) -> str:
    separators = [" ", ".", "_", "-"]
    scores = [torrent_name.count(sep) for sep in separators]
    return separators[scores.index(max(scores))]


def normalize(name: str) -> str:
    # Clean name using predefined regex patterns
    name = re.sub(video_qualities_rgx, "", name)
    name = re.sub(audio_qualities_rgx, "", name)
    name = re.sub(link_ads_rgx, "", name)
    name = re.sub(movie_version_rgx, "", name)

    # Guess and clean separator
    sep = guess_separator(name)
    # Replace multiple variants of separators with a single space
    name = re.sub(rf"\s*{re.escape(sep)}\s*", " ", name)
    if sep != "-":
        name = re.sub(r"\s+-\s+", " ", name)

    # Clean up extra spaces between words
    name = re.sub(r"\s{2,}", " ", name)

    # Clean unnecessary parenthesis and brackets
    name = re.sub(r"\(\s*\)", "", name)
    name = re.sub(r"\[\s*\]", "", name)

    # Return the normalized name (lowercased and stripped of leading/trailing spaces)
    return name.lower().strip()


def get_movie_title(normalized_name: str) -> str:
    match = movie_rgx.match(normalized_name)
    if match is None:
        return normalized_name

    title = normalized_name[match.start() : match.end()].strip()

    # normalize year to be like this: (1970)
    if wrapped_year_rgx.search(title) is None:
        match = list(year_rgx.finditer(title))
        last_match = match[-1]
        year = title[last_match.start() : last_match.end()]
        title = (
            title[: last_match.start()] + "(" + year + ")" + title[last_match.end() :]
        )

    return title


def get_tvshow_title(normalized_name: str) -> str:
    match = tvshow_rgx.match(normalized_name)
    if match is None:
        return normalized_name

    normalized = normalized_name[match.start() : match.end()].strip()

    match = re.search(r"s(eason)?(\.|\s)?0?(?P<season>\d+)", normalized, re.IGNORECASE)
    if match is None:
        return normalized_name

    title = normalized[0 : match.start()].strip()
    return f"{title} s{match.group('season')}"


def get_year_from_movie(normalized_name: str) -> str | None:
    matches = year_rgx.search(normalized_name)
    if matches is None:
        return None
    return re.sub(r"\D", "", matches[0])  # keep only digits


class TorrentWrapper:
    """
    A torrent can be a movie or a tvshows (it could be music album too, but that's another story).
    This class keep every info about the torrent.
    """

    def __init__(self, torrent: Torrent) -> None:
        self._original_name = torrent.name
        self._normalized_name = normalize(self._original_name)

        self._is_music = False
        self._is_movie = False
        self._is_tvshow = False

        candidates = [f for f in torrent.get_files() if f.selected]

        self._is_music = (
            len([c for c in candidates if Path(c.name).suffix in music_extensions]) > 0
        )

        self._files = [f for f in candidates]

        if not self._is_music:
            if len(self._files) == 1:
                self._is_movie = True
            elif len(self._files) > 1:
                self._is_tvshow = True

    @property
    def original_name(self) -> str:
        return self._original_name

    @property
    def normalized_name(self) -> str:
        return self._normalized_name

    @property
    def title(self) -> str:
        if self.is_tvshow():
            return get_tvshow_title(self.normalized_name)
        elif self.is_movie():
            return get_movie_title(self.normalized_name)
        return self.normalized_name

    @property
    def year(self) -> str | None:
        if not self.is_movie():
            return None
        match = get_year_from_movie(self.normalized_name)
        # if match is None:
        #     raise ValueError(f"No year for this movie: {self.original_name}")
        return match

    @property
    def size_in_mega_bytes(self) -> int:
        size = sum(f.size for f in self._files)
        return size // (1024**2)

    @property
    def size_in_giga_bytes(self) -> int:
        size = sum(f.size for f in self._files)
        return size // (1024**3)

    def is_movie(self) -> bool:
        return self._is_movie

    def is_tvshow(self) -> bool:
        return self._is_tvshow

    def is_music(self) -> bool:
        return self._is_music

    def __str__(self) -> str:
        return self.original_name

    def __repr__(self) -> str:
        return self.original_name

    def __hash__(self) -> int:
        return hash(self.original_name)

    def __eq__(self, other: object) -> bool | NotImplementedType:
        if not isinstance(other, TorrentWrapper):
            return NotImplemented
        return hash(self) == hash(other)
