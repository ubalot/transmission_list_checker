let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  packages = [
    (with pkgs; [
      python3
      uv
    ])
  ];
  LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib";
}
